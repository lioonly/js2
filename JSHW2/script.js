// 1) String,Boolean,Undefined,Null,Number,Object
// 2) == - рiвнiсть, не звертає увагу на типи даних. Тобто "1" == 1 буде `true`. === - строга рiвнiсть, порівнює значення змінних і їхні типи даних.Тобто "1" === 1 буде `false`.
// 3) Оператори - це символи або ключові слова, які виконують певні дії над операндами.

function showConfirmation(name) {
    return confirm(`Are you sure you want to continue, ${name}?`);
  }
  
  function showMessage(message) {
    alert(message);
  }
  
  function getUserData() {
    const name = prompt("Please enter your name:", localStorage.getItem("name") || "");
    const age = prompt("Please enter your age:", localStorage.getItem("age") || "");
  
    if (!name || !age || isNaN(age)) {
      return getUserData();
    }
  
    localStorage.setItem("name", name);
    localStorage.setItem("age", age);
  
    return { name: name, age: Number(age) };
  }
  
  const userData = getUserData();
  const name = userData.name;
  const age = userData.age;
  
  if (age < 18) {
    showMessage("You are not allowed to visit this website.");
  } else if (age >= 18 && age <= 22) {
    const confirmed = showConfirmation(name);
    if (confirmed) {
      showMessage(`Welcome, ${name}!`);
    } else {
      showMessage("You are not allowed to visit this website.");
    }
  } else {
    showMessage(`Welcome, ${name}!`);
  }